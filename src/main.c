//
// Created by vlad on 12/11/22.
//

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void successful_memalloc_test();
static void freeing_heap_block_test();
static void freeing_two_blocks_test();
static void out_of_memory_with_heap_expanding_test();
static void out_of_memory_with_heap_expanding_test2();

int main() {
    successful_memalloc_test();
    freeing_heap_block_test();
    freeing_two_blocks_test();
    freeing_two_blocks_test();
    out_of_memory_with_heap_expanding_test();
    out_of_memory_with_heap_expanding_test2();
    return 0;
}

static struct block_header *header_of(void *content_addr) {
    return (struct block_header *)(((uint8_t *)content_addr) - offsetof(struct block_header, contents));
}

static void* create_heap(size_t size) {
    void* heap = heap_init(size);
    if (heap)
        msg_log("heap created");
    else
        error_log("heap creation error");
    return heap;
}

static void* create_block(size_t size) {
    void* block = _malloc(size);
    if (block)
        msg_log("block allocated");
    else
        error_log("allocation error");
    return block;
}

static bool test_heap_of_size(size_t size) {
    void* heap = create_heap(size);
    if (!heap)
        return false;
    debug_heap(stdout, heap);
    void *block1 = create_block(256);
    if (!block1 || header_of(block1)->is_free) {
        error_log("memalloc error");
        return false;
    }
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    return true;
}

static void successful_memalloc_test() {
    print_frame("TEST 1", "successful memalloc test");
    msg_log("creating heap of 128 bytes:");
    if (test_heap_of_size(128))
        print_line("SUCCESS TEST 1");
    else
        error_log("error");
}

static void freeing_heap_block_test() {
    print_frame("TEST 2", "freeing 1 heap block test");
    void *heap = create_heap(1024);
    if (!heap)
        return;
    debug_heap(stdout, heap);
    msg_log("block 1 allocation (256)");
    void *block1 = create_block(256);
    msg_log("block 2 allocation (64)");
    void *block2 = create_block(64);
    msg_log("block 3 allocation (512)");
    void *block3 = create_block(512);
    debug_heap(stdout, heap);
    struct block_header *block2_h = header_of(block2);
    _free(block2);
    if (!block2_h->is_free) {
        error_log("block 2 freeing error");
        return;
    }
    msg_log("block 2 freed");
    debug_heap(stdout, heap);
    if (!header_of(block1) || !header_of(block3)) {
        error_log("other blocks are damaged");
        return;
    }
    print_line("SUCCESS TEST 2");
    munmap(heap, REGION_MIN_SIZE);
}

static void freeing_two_blocks_test() {
    print_frame("TEST 3", "freeing 2 heap blocks test");
    void *heap = create_heap(1024);
    if (!heap)
        return;
    debug_heap(stdout, heap);
    msg_log("block 1 allocation (256)");
    void *block1 = create_block(256);
    msg_log("block 1 allocation (64)");
    void *block2 = create_block(64);
    msg_log("block 1 allocation (512)");
    void *block3 = create_block(512);
    debug_heap(stdout, heap);
    struct block_header *block2_h = header_of(block2);
    struct block_header *block3_h = header_of(block3);
    _free(block2);
    _free(block3);
    if (!block2_h->is_free || !block3_h->is_free) {
        error_log("block freeing error");
        return;
    }
    msg_log("block2 and block3 are freed");
    debug_heap(stdout, heap);
    if (!header_of(block1)) {
        error_log("other blocks are damaged");
        return;
    }
    print_line("SUCCESS TEST 3");
    munmap(heap, REGION_MIN_SIZE);
}


static void out_of_memory_with_heap_expanding_test() {
    print_frame("TEST 4", "out of memory with expanding heap test");
    void *heap = create_heap(REGION_MIN_SIZE);
    if (!heap)
        return;
    debug_heap(stdout, heap);
    void *big_block = create_block(REGION_MIN_SIZE+100);
    if (!big_block)
        return;
    debug_heap(stdout, heap);
    _free(big_block);
    msg_log("freed successfully\n");
    debug_heap(stdout, heap);
    munmap(heap, 2*REGION_MIN_SIZE);
    print_line("SUCCESS TEST 4");
}


static void out_of_memory_with_heap_expanding_test2() {
    print_frame("TEST 5", "out of memory with expanding heap test (new region too big to continue previous)");
    void *heap = create_heap(REGION_MIN_SIZE);
    if (!heap)
        return;
    debug_heap(stdout, heap);
    void *block1 = create_block(6000);
    if (!block1)
        return;
    msg_log("first block created");
    debug_heap(stdout, heap);
    struct block_header *block_h = header_of(block1);
    (void)mmap(block_h->contents + block_h->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    msg_log("trying to create second block...");
    void *block2 = _malloc(6000);
    if (!block2)
        return;
    debug_heap(stdout, heap);
    _free(block1);
    _free(block2);
    msg_log("blocks was freed");
    munmap(heap, REGION_MIN_SIZE);
    munmap(header_of(block2), REGION_MIN_SIZE);
    print_line("SUCCESS TEST 5");
}
