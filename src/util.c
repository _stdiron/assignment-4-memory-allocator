#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args); // NOLINT 
  va_end (args);
  abort();
}


extern inline size_t size_max( size_t x, size_t y );

void msg_log(const char* msg) {
    printf("MESSAGE: %s\n", msg);
}

void error_log(const char* msg) {
    printf("ERROR: %s\n", msg);
}

void print_frame(const char* header, const char* msg) {
    size_t header_len = strlen(header);
    char lower_frame[21+header_len];
    for (int i = 0; i < header_len+20; i++)
        lower_frame[i] = '-';
    lower_frame[20+header_len] = 0;
    printf("--------- %s ---------\n%s\n%s\n", header, msg, lower_frame);
}

void print_line(const char* header) {
    printf("--------- %s ---------\n", header);
}